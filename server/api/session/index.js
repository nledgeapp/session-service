'use strict';

var express = require('express');
var controller = require('./session.controller');
var router = express.Router();

router.get('/getAllSessions', controller.getAllSessions);
router.get('/getSession', controller.getSession);

module.exports = router;
