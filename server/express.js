'use strict';

var express = require('express'),
    compression = require('compression'),
    methodOverride = require('method-override'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser')

module.exports = function(app) {
    var env = app.get('env');
    app.use(compression());
    app.use(cookieParser());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    app.use(methodOverride());
};
