'use strict';

var path    = require('path'),
    _       = require('lodash'),
    appRoot = path.normalize(__dirname + '/../../../')

// All configurations will extend these options
// ============================================
var all = {
env: process.env.NODE_ENV || 'localhost',

// Root path of server
root:appRoot,

// Server port
port: process.env.PORT || 9000,

};

// Export the config object based on the NODE_ENV and local config
// ============================================================================
var envConfig = require('./conf-' + all.env + '.js'),
args = _.flatten([all, envConfig]);

module.exports = _.merge.apply(_, args);
