process.env.NODE_ENV = process.env.NODE_ENV || 'localhost'
var express = require('express'),
    config = require('./config'),
    log = require('bog'),
    // Setup server
    app = express(),
    http = require('http'),
    server = null

server = http.createServer(app).listen(config.port)
require('./express')(app);
require('./routes')(app);

    // Start server
server.listen(config.port, function () {
    log.info('Express server listening on PORT:',config.port,'in',app.get('env'),'mode');
});

// Expose app
exports = module.exports = app;
